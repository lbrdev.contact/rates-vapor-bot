//
//  Controller.swift
//
//
//  Created by Ihor Kandaurov on 27.01.2023.
//

import AsyncHTTPClient
import Dispatch
import Foundation
import SwiftSoup
import Vapor

public class Controller {
    private let timer: DispatchSourceTimer
    
    private var lastSentDate: Date?
    private let idleMessageMinutes = 60
    private let attemptSeconds = 60 * 5
    
    private let httpClient = HTTPClient(eventLoopGroupProvider: .createNew)
    private var lastRate: Decimal?

    private let url: String
    private let telegramKey: String
    private let chatId: String
    private let logger = Logger(label: "rates_logger")

    public init(
        url: String,
        telegramKey: String,
        chatId: String
    ) {
        self.url = url
        self.telegramKey = telegramKey
        self.chatId = chatId
        timer = DispatchSource.makeTimerSource()

        timer.setEventHandler {
            self.attempt()
        }

        timer.schedule(deadline: .now(), repeating: .seconds(attemptSeconds), leeway: .seconds(1))
    }

    public func start() {
        timer.activate()
    }

    private func attempt() {
        Task.init {
            do {
                let rate = try await fetchRate()
                if let message = makeMessage(rate) {
                    try await sendMessage(message)
                }
            } catch {
                logger.error("\(error.localizedDescription)")
            }
        }
    }

    private func makeMessage(_ rate: Decimal) -> String? {
        guard rate != lastRate else {
            logger.info("rate is the same, skip...")
            return nil
        }
        lastRate = rate

        let usd = 1000 * rate
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.currencyCode = "USD"
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "uk_UA")

        let text = "▶︎ 1000 USDT = \(formatter.string(for: usd) ?? "\(usd)") "
        return text
    }

    private func fetchRate() async throws -> Decimal {
        var request = HTTPClientRequest(url: url)
        request.method = .GET

        let response = try await httpClient.execute(request, timeout: .seconds(30))
        let body = try await response.body.collect(upTo: 1024 * 1024)

        guard response.status == .ok else { throw FetchError.notFound }
        
        logger.info("rate is fetched")

        let doc = try SwiftSoup.parse(String(buffer: body))

        let result = try doc.getElementsByClass("give__course-value").text()
        guard !result.isEmpty else {
            throw FetchError.notTagFound
        }
        
        let last = result.components(separatedBy: " ").last

        guard let last, let amount = Decimal(string: last) else {
            throw FetchError.unableToMap
        }

        logger.info("rate is \(amount)")

        return amount
    }

    private func sendMessage(_ message: String) async throws {
        logger.info("sending to channel...")
        let properMessage = message.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = HTTPClientRequest(
            url: "https://api.telegram.org/bot\(telegramKey)/sendMessage?chat_id=@\(chatId)&text=\(properMessage ?? "")"
        )
        request.method = .POST

        let response = try await httpClient.execute(request, timeout: .seconds(30))

        if response.status == .ok {
            lastSentDate = Date()
            logger.info("sent to channel")
        } else {
            logger.info("\(response.status)")
            logger.info("sending to channel... failed")
        }
    }
}

enum FetchError: Error {
    case notFound
    case unableToMap
    case notTagFound
}
