import SwiftSoup
import Vapor

func routes(_ app: Application) throws {
    app.get { req async throws in
        "ok"
    }
}

enum ScrapeError: Error {
    case documentIsEmpty
    case tagNotFound
    case attributeNotFound(_: String)
}
